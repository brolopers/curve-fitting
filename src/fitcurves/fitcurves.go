package fitcurves

import (
	"math"
)

type fitInput struct {
	Function 	int
	Name     	string
	RawX     	[]float64
	RawY     	[]float64
}

type fitOutput struct {
	ScaledX    	[]float64
	ScaledY    	[]float64
	Name       	string
	FittedPars 	[]float64
	ScaleX     	float64
	ScaleY     	float64
	Residual   	float64
}

type CurveFit struct {
	Output 		fitOutput
	Input  		fitInput
	Funcs  		[]func([]float64, float64) float64
}

func (v *CurveFit) Logistic(k []float64, x float64) float64 {
	return k[0] / (1 + k[1]*(math.Pow(x+0.001, k[2])))
}

func (v *CurveFit) Exponential(k []float64, x float64) float64 {
	return k[0] / (1 + k[1]/(x+0.001))
}

func (v *CurveFit) NegativeExponential(k []float64, x float64) float64 {
	return k[0] * (1 - math.Exp(-k[1]*x))
}

func (v *CurveFit) Initialise() {
	max1 := max(v.Input.RawX)
	max2 := max(v.Input.RawY)
	scaleX := math.Pow(10, float64(math.Ceil(math.Log10(max1))))
	scaleY := math.Pow(10, float64(math.Ceil(math.Log10(max2))))

	v.Output.ScaleX = scaleX
	v.Output.ScaleY = scaleY
	v.Output.Name = v.Input.Name
	v.Output.ScaledX = fmap(func(x float64) float64 { return x / scaleX }, v.Input.RawX)
	v.Output.ScaledY = fmap(func(x float64) float64 { return x / scaleY }, v.Input.RawY)
	v.Funcs = []func([]float64, float64) float64{v.Logistic, v.Exponential, v.NegativeExponential}
}

func fmap(f func(float64) float64, data []float64) []float64 {
	output := make([]float64, len(data))
	for i, _ := range data {
		output[i] = f(data[i])
	}
	return output
}

func max(data []float64) float64 {
	output := -math.Pow(100, 100)
	for i, _ := range data {
		if data[i] > output {
			output = data[i]
		}
	}
	return output
}

func norm(data []float64) []float64 {
	output := make([]float64, len(data))
	for i, _ := range data {
		if data[i] != 0 {
			output[i] = data[i] / math.Pow(math.Pow(data[i], 2), 0.5)
		} else {
			output[i] = 0
		}
	}

	return output
}

func sum(data []float64) float64 {
	output := 0.0
	for i, _ := range data {
		output += data[i]
	}
	return output
}

func zip(f func(float64, float64) float64, data1 []float64, data2 []float64) []float64 {
	output := make([]float64, len(data1))
	for i, _ := range data1 {
		output[i] += f(data1[i], data2[i])
	}
	return output
}

func (v *CurveFit) bisect(upperBound float64, f func(float64) float64) float64 {
	fDX := upperBound
	fBis := 0.0
	fMid := 0.0
	fMidEval := 0.0

	for i := 0; i < 50; i++ {
		fDX *= 0.5
		fMid = fBis + fDX
		fMidEval = f(fMid)
		if fMidEval < 0.0 {
			fBis = fMid
		}
	}

	return fMid
}

func (v *CurveFit) FitPoints() []float64 {
	rawx := v.Input.RawX
	rawy := v.Input.RawY
	derr := 0.0000001
	output := make([]float64, 3)

	x1, x2, y1, y2 := 0.0, 0.0, 0.0, 0.0

	if len(rawx) > 1 {
		x1 = rawx[int((len(rawx)-1)/2)] / v.Output.ScaleX
		x2 = rawx[len(rawx)-1] / v.Output.ScaleX
		y1 = rawy[int((len(rawx)-1)/2)] / v.Output.ScaleY
		y2 = rawy[len(rawx)-1] / v.Output.ScaleY
	} else {

		return output
	}

	switch {

		case 0 == v.Input.Function:
			b := (y1/(y2+derr) - 1) / (1/(x2+derr) - y1/(y2+derr)*1/(x1+derr))
			a := y1 * (1 + b/(x1+derr))
			c := -1.0
			//possibly negative curve

			if a < 0 {
				a = y2
				b = x1 * (a/y1 - 1)
			}
			output[0], output[1], output[2] = a, b, c

		case 1 == v.Input.Function:
			//possibly change this
			//same as logistic put c parameter not used
			b := (y1/(y2+derr) - 1) / (1/(x2+derr) - y1/(y2+derr)*1/(x1+derr))
			a := y1 * (1 + b/(x1+derr))
			c := -1.0
			//possibly negative curve
			if a < 0 {
				a = y2
				b = x1 * (a/y1 - 1)
			}
			output[0], output[1], output[2] = a, b, c

		case 2 == v.Input.Function:
			f := func(x float64) float64 { return (1-math.Exp(-x*x1))/(1-math.Exp(-x*x2)+derr) - y1/(y2+derr) }
			b := v.bisect(10000, f)
			a := y1 / (1 - math.Exp(-b*x1) + derr)
			c := -1.0

			output[0], output[1], output[2] = a, b, c
	}

	return output
}

func (v *CurveFit) Fit() []float64 {

	f := v.Residual
	z := v.FitPoints()

	vals := []float64{0.0001, 0.001, 0.01, 0.1, 1.0}
	bestz := f(z)

	for t := 0; t < 50000; t++ {
		besti := -1
		g := v.gradient(z, f)

		for i, _ := range vals {
			z = zip(func(x float64, y float64) float64 { return x - vals[i]*y }, z, g)

			value := f(z)
			if value < bestz {
				bestz = value
				besti = i
			}

			z = zip(func(x float64, y float64) float64 { return x + vals[i]*y }, z, g)

		}
		if besti != -1 {
			z = zip(func(x float64, y float64) float64 { return x - vals[besti]*y }, z, g)
		}

	}

	v.Output.FittedPars = z
	v.Output.Residual = v.Residual(z)

	return z
}

func (v *CurveFit) Residual(k []float64) float64 {
	f := v.Funcs[v.Input.Function]

	rawx := v.Output.ScaledX
	rawy := v.Output.ScaledY

	model := fmap(func(x float64) float64 { return f(k, x) }, rawx)

	r := zip(func(x float64, y float64) float64 { return math.Pow(x-y, 2) }, rawy, model)

	return sum(r)
}

func (v *CurveFit) ResidualAbs(k []float64) float64 {
	f := v.Funcs[v.Input.Function]

	rawx := v.Output.ScaledX
	rawy := v.Output.ScaledY

	model := fmap(func(x float64) float64 { return f(k, x) }, rawx)

	r := zip(func(x float64, y float64) float64 { return math.Abs(x - y) }, rawy, model)

	return sum(r)
}

func (v *CurveFit) Calculate(data []float64) []float64 {
	f := v.Funcs[v.Input.Function]

	k := v.Output.FittedPars

	data = fmap(func(x float64) float64 { return x / v.Output.ScaleX }, data)
	model := fmap(func(x float64) float64 { return f(k, x) }, data)
	model = fmap(func(x float64) float64 { return v.Output.ScaleY * x }, model)

	return model
}

func (v *CurveFit) gradient(p []float64, f func([]float64) float64) []float64 {
	const dP = 0.00001

	output := make([]float64, len(p))

	for i, _ := range output {
		fp := f(p)
		p[i] += dP
		fpdp := f(p)
		p[i] -= dP
		output[i] = (fpdp - fp) / dP
	}
	return output
}
