package fitcurves

import (
	"testing"
	"encoding/csv"
	"os"
	"strconv"
	"log"
)

func ReadCSVQuick(fileName string) ([][]float64, [][]float64, []float64) {

	csvFile, _ := os.Open(fileName)
	r := csv.NewReader(csvFile)
	records, _ := r.ReadAll()

	outputX := make([][]float64, len(records))
	outputY := make([][]float64, len(records))
	models := make([]float64, len(records))

	for row, _ := range records {

		models[row], _ = strconv.ParseFloat(records[row][0], 64)
		size, _ := strconv.ParseFloat(records[row][1], 64)

		x := make([]float64, int(size))
		y := make([]float64, int(size))

		for i, v := range records[row][2 : 2+int(size)] {
			x[i], _ = strconv.ParseFloat(v, 64)
		}
		for i, v := range records[row][2+int(size) : 2+2*int(size)] {
			y[i], _ = strconv.ParseFloat(v, 64)
		}

		outputX[row] = make([]float64, int(size))
		outputY[row] = make([]float64, int(size))
		copy(outputX[row], x)
		copy(outputY[row], y)
	}

	return outputX, outputY, models
}

func TestFit(t *testing.T) {

	xvalues, yvalues, models := ReadCSVQuick("fit_file.csv")

	cf := new(CurveFit)
	cf.Input.Name = "Test_fit"

	for row, _ := range xvalues {
		cf.Input.RawX = xvalues[row]
		cf.Input.RawY = yvalues[row]
		cf.Input.Function = int(models[row])
		cf.Initialise()
		cf.Fit()
		log.Println(cf.Output.FittedPars, cf.ResidualAbs(cf.Output.FittedPars), cf.Output.ScaleX, cf.Output.ScaleY)
	}
}

func TestClient(t *testing.T) {

	xvalues, yvalues, models := ReadCSVQuick("fit_file_Client.csv")

	cf := new(CurveFit)
	cf.Input.Name = "Test_client"

	for row, _ := range xvalues {
		cf.Input.RawX = xvalues[row]
		cf.Input.RawY = yvalues[row]
		cf.Input.Function = int(models[row])
		cf.Initialise()
		cf.Fit()
		log.Println(cf.Output.FittedPars, cf.ResidualAbs(cf.Output.FittedPars), cf.Output.ScaleX, cf.Output.ScaleY)
	}

}

func TestClient2(t *testing.T) {

	xvalues, yvalues, models := ReadCSVQuick("fit_file_Client2.csv")

	cf := new(CurveFit)
	cf.Input.Name = "Test_client2"

	for row, _ := range xvalues {
		cf.Input.RawX = xvalues[row]
		cf.Input.RawY = yvalues[row]
		cf.Input.Function = int(models[row])
		cf.Initialise()
		cf.Fit()
		log.Println(cf.Output.FittedPars, cf.ResidualAbs(cf.Output.FittedPars), cf.Output.ScaleX, cf.Output.ScaleY)
	}

}

func TestClient3(t *testing.T) {

	xvalues, yvalues, models := ReadCSVQuick("fit_file_Client3.csv")

	cf := new(CurveFit)
	cf.Input.Name = "Test_client3"

	for row, _ := range xvalues {
		cf.Input.RawX = xvalues[row]
		cf.Input.RawY = yvalues[row]
		cf.Input.Function = int(models[row])
		cf.Initialise()
		cf.Fit()
		log.Println(cf.Output.FittedPars, cf.ResidualAbs(cf.Output.FittedPars), cf.Output.ScaleX, cf.Output.ScaleY, cf.Calculate(cf.Input.RawX))
	}
}